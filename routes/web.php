<?php

use Inertia\Inertia;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Application;
use App\Http\Controllers\KtpController;
use App\Http\Controllers\MasterController;
use App\Http\Controllers\ModuleController;
use App\Http\Controllers\KabukotController;
use App\Http\Controllers\PropinsiController;
use App\Http\Controllers\KecamatanController;
use App\Http\Controllers\KelurahanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');
});

Route::middleware(['auth:sanctum',config('jetstream.auth_session'),'verified',])->group(function () {
    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');
});

// Route::middleware(['auth:sanctum','verified',])->resource('users', UserController::class);

Route::group(['prefix' => 'learn', 'middleware' => ['auth:sanctum','verified']], function(){
    Route::middleware(['auth:sanctum','verified',])->resource('master', MasterController::class);
    Route::middleware(['auth:sanctum','verified',])->resource('module', ModuleController::class);
    Route::middleware(['auth:sanctum','verified',])->resource('ktp', KtpController::class);

    Route::get('/kabukot/{id_prop}', [KabukotController::class, 'getKabukot'])->name('getkabukot');
    Route::get('/kecamatan/{id_kabkota}', [KecamatanController::class, 'getDatakecamatan'])->name('getkecamatan');
    Route::get('/kelurahan/{id_kec}', [KelurahanController::class, 'getDatakelurahan'])->name('getkelurahan');
    Route::get('/propinsi', [PropinsiController::class, 'index'])->name('propinsi');
});



// Route::middleware(['auth:sanctum', 'verified'])->group(function () {

//    Route::get('/test/{id_prop}', [KtpController::class, 'getKabukot'])->name('getkabukot');
// });
