<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_tmst_propinsi', function (Blueprint $table) {
            $table->id();
            $table->id('id_prop')->autoIncrement(false);
            $table->string('name');
            $table->string('description')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
            $table->integer('moduser')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_tmst_propinsi');
    }
};
