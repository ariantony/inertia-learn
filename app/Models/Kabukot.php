<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kabukot extends Model
{
    use HasFactory;
    public $table = 'sys_tmst_kabukot';
    public $timestamps = true;

    protected $fillable = [
        'id',
        'id_prop',
        'id_kabkota',
        'name',
        'description',
        'status',
        'created_at',
        'updated_at',
        'moduser',
        'deleted_at',
    ];

    protected $guarded = [

    ];

    protected $hidden = [


    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s'
    ];
}
