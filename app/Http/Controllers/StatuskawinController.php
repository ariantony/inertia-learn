<?php

namespace App\Http\Controllers;

use App\Models\Statuskawin;
use Illuminate\Http\Request;

class StatuskawinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Statuskawin  $statuskawin
     * @return \Illuminate\Http\Response
     */
    public function show(Statuskawin $statuskawin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Statuskawin  $statuskawin
     * @return \Illuminate\Http\Response
     */
    public function edit(Statuskawin $statuskawin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Statuskawin  $statuskawin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Statuskawin $statuskawin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Statuskawin  $statuskawin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Statuskawin $statuskawin)
    {
        //
    }
}
