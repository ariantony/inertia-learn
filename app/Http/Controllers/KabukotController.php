<?php

namespace App\Http\Controllers;

use App\Http\Resources\KabukotResource;
use Inertia\Inertia;
use App\Models\Kabukot;
use Illuminate\Http\Request;

class KabukotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getKabukot($id_prop)
    {
        $data_kabukot = Kabukot::where('id_prop','=', $id_prop)->get();
        return KabukotResource::collection($data_kabukot);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kabkota  $kabkota
     * @return \Illuminate\Http\Response
     */
    public function show(Kabkota $kabkota)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kabkota  $kabkota
     * @return \Illuminate\Http\Response
     */
    public function edit(Kabkota $kabkota)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kabkota  $kabkota
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kabkota $kabkota)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kabkota  $kabkota
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kabkota $kabkota)
    {
        //
    }
}
