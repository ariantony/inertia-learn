<?php

namespace App\Http\Controllers;

use App\Models\Kacamatan;
use App\Models\Kecamatan;
use Illuminate\Http\Request;
use App\Http\Resources\KecamatanResource;

class KecamatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getDatakecamatan($id_kabkota)
    {
        $data_kecamatan = Kecamatan::where('id_kabkota','=', $id_kabkota)->get();
        return KecamatanResource::collection($data_kecamatan);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kacamatan  $kacamatan
     * @return \Illuminate\Http\Response
     */
    public function show(Kacamatan $kacamatan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kacamatan  $kacamatan
     * @return \Illuminate\Http\Response
     */
    public function edit(Kacamatan $kacamatan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kacamatan  $kacamatan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kacamatan $kacamatan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kacamatan  $kacamatan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kacamatan $kacamatan)
    {
        //
    }
}
