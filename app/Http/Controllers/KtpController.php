<?php

namespace App\Http\Controllers;

use App\Models\Agama;
use App\Models\Bloodtype;
use App\Models\Kabukot;
use App\Models\Ktp;
use App\Models\Propinsi;
use App\Models\Sex;
use Inertia\Inertia;
use Illuminate\Http\Request;

class KtpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Master/Ktp/Index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(
            Sex $sex,
            Bloodtype $bloodtype,
            Agama $agama,
            Propinsi $propinsi,
    )
    {
        $sex = Sex::all('id','name');
        $nationality = [['id' => '1', 'name' => 'WNI'],['id' => '2', 'name' => 'WNA']];
        $bloodtype = Bloodtype::all('id','name');
        $agama = Agama::all('id','name');
        $propinsi = Propinsi::all('id','id_prop','name');

        return Inertia::render('Master/Ktp/Create',[
            'sexes' => $sex,
            'blood_types' => $bloodtype,
            'religions' => $agama,
            'nationalities' => $nationality,
            'data_propinsi' => $propinsi
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    // public function getKabukot(Kabukot $kabukot, $id_prop)
    // {
    //     $kabukot = Kabukot::where('id_prop','=', $id_prop);

    //     return $kabukot;
    // }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ktp  $ktp
     * @return \Illuminate\Http\Response
     */
    public function show(Ktp $ktp)
    {
        return Inertia::render('Master/Ktp/View');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ktp  $ktp
     * @return \Illuminate\Http\Response
     */
    public function edit(Ktp $ktp)
    {
        return Inertia::render('Master/Ktp/Edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ktp  $ktp
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ktp $ktp)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ktp  $ktp
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ktp $ktp)
    {
        //
    }
}
